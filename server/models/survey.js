'use strict';
var loopback = require("loopback");
var app = require('../../server/server');
var crypto = require('crypto');
var Bitly = require('bitly');
var bitly = new Bitly('23fc8e4c0b6a18f8a94d2256fea741f1dc12abd1');
function createRandomString() {
  var current_date = (new Date()).valueOf().toString();
  var random = Math.random().toString();
  return crypto.createHash('sha1').update(current_date + random).digest('hex');
}
module.exports = function (Survey) {
  Survey.registration = function (data, cb) {
    var fbData = {
      name: "Facebook collector",
      url: createRandomString(),
      isFb: true
    };
    var twitterData = {
      name: "Twitter collector",
      url: createRandomString(),
      isTwitter: true
    };
    var LinkedInData = {
      name: "LinkedIn collector",
      url: createRandomString(),
      isLinkedIn: true
    };
    var ViadeoData = {
      name: "Viadeo collector",
      url: createRandomString(),
      isViadeo: true
    };
    var webData = {
      name: "Web collector",
      url: createRandomString()
    };
    app.models.collector.create([webData, fbData, twitterData, LinkedInData, ViadeoData], function (err, res) {
      if (err) {
        throw err;
      }
      else {
        for (let i = 0; i < res.length; i++) {

          data.collectors.push(res[i])
        }
        var survey = new Survey(data);
        survey.save(function (err, surveyData) {
          if (err) {
            throw err;
          }
          app.models.account.findById(data.accountId, function (err, accountData) {
            if (err) {
              throw err;
            }
            else {
              accountData.surveys.push(surveyData.id);
              accountData.save(function (err, accountInfo) {
                if (err) {
                  throw err;
                }
                else {
                  for (let i = 0; i < survey.collectors.length; i++) {
                    var id = survey.collectors[i].id;
                    var url = survey.collectors[i].url;
                    var baseUrl = 'https://demo2apps.herokuapp.com/'
                    var b = baseUrl + "/survey/answer/" + id + "/" + url;
                    bitly.shorten(b)
                      .then(function (result) {
                        survey.collectors[i].shortUrl = result.data.url;
                        survey.collectors[i].longUrl = result.data.long_url;
                        survey.save(function (err) {
                          if (err) {
                            throw err;
                          }
                          else {

                          }
                        })
                      })
                  }
                  let obj={ message:"survey created sucessfully"}
                  cb(null, obj)
                }
              })
            }
          })
        })
      }
    });
  };
  Survey.remoteMethod(
    'registration',
    {
      description: 'Create new survey ',
      accepts: { arg: 'data', type: 'object', http: { source: 'body' } },
      http: { path: '/new/survey/registration', verb: 'post' },
      returns: { arg: 'result', type: 'Array' }
    });
  Survey.getSurveyInfo = function (id, cb) {
    app.models.account.findById(id, function (err, account) {
      if (err) {
        throw err;
      }
      else {
        Survey.find({
          where: {
            "_id": {
              inq: account.surveys
            }
          }
        }, function (err, survey) {
          if (err) {
            throw err;
          }
          else {
            cb(null, survey);
          }
        });
      };
    })
  }
  Survey.remoteMethod('getSurveyInfo', {
    description: 'Get list of surveys on userId basis',
    accepts: { arg: 'id', type: 'any', required: true },
    http: { path: '/all/survey/user/:id', verb: 'get' },
    returns: { arg: 'result', type: 'Object' }
  });
  Survey.getSurveyBYId = function (id, cb) {
    Survey.findById(id, function (err, survey) {
      if (err) {
        throw err;
      }
      else {
        cb(null, survey)
      };
    })
  }
  Survey.remoteMethod('getSurveyBYId', {
    description: 'Get survey by survey id',
    accepts: { arg: 'id', type: 'any', required: true },
    http: { path: '/get/survey/:id', verb: 'get' },
    returns: { arg: 'result', type: 'Object' }
  });
  Survey.deleteSurvey = function (surveyId, accountId, cb) {
    app.models.account.findById(accountId, function (err, account) {
      if (err) {
        throw err;
      }
      else {
        for (let i = 0; account.surveys.length; i++) {
          if (account.surveys[i] == surveyId) {
            var index = i;
            break;

          }
        }
        account.surveys.splice(index, 1);
        account.save(function (err, done) {
          if (err) {
            throw err;
          }
          else {
            Survey.destroyById(surveyId, function (err, surveys) {
              if (err) {
                throw err;
              }
              cb(null, surveys);
            })
          }
        })
      };
    })
  }

  Survey.remoteMethod('deleteSurvey', {
    description: 'Delete an survey',
    accepts: [
      { arg: 'surveyId', type: 'any', required: true },
      { arg: 'accountId', type: 'any', required: true }
    ],
    http: { path: '/delete/survey/:surveyId/:accountId', verb: 'delete' },
    returns: { arg: 'result', type: 'Object' }
  });
  Survey.updateSurvey = function (id, data, cb) {
    Survey.update({ id: id }, data, function (err, res) {
      if (err) {
        throw err;
      }
      else {
        cb(null, res);
      }
    });
  };
  Survey.remoteMethod('updateSurvey', {
    description: 'uodate survey by id',
    accepts: [
      { arg: 'id', type: 'any', required: true },
      { arg: 'data', type: 'object', required: true, http: { source: 'body' } }
    ],
    http: { path: '/new/survey/update/:id', verb: 'put' },
    returns: { arg: 'result', type: 'Object' }
  });
}