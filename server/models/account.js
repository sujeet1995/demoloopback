'use strict';
var http = require("http");
var loopback = require("loopback");
var path = require('path');
var LoopBackContext = require('loopback-context');
var app = require('../../server/server');
module.exports = function(Account) {
  Account.registration=function(data, cb) {
  	if(data.role_id == "5acddd59f36d285dd8f27d13"){
  		let userDetail = {
               email:data.email,
               first_name:data.first_name,
               last_name:data.last_name,
               password:data.password,
               role_id:data.role_id,
               date_created:Date.now
           }
           Account.create(userDetail, function(err,userInfo){
               if (err) { cb(null, err);}
               else{
               	Account.app.models.RoleMapping.create({principalType: 'USER', principalId: userInfo._id, roleId: userInfo.role_id });
                var myMessage = {heading:"Welcome to Spoking", text:"We are happy to have you on board."}; 
                var renderer = loopback.template(path.resolve(__dirname, '../../server/views/welcome.ejs'));
                var html_body = renderer(myMessage);
                loopback.Email.send({
                    to: userInfo.email,
            				from: "clientes@woowbolivia.com",
            				subject: "subject",
            				text: "text message",
            				html: html_body,
            				//attachments : [path.resolve('../client/images/an-image.jpg')],
            				var : {
            				    myVar1 : 'a custom value'
            				},
            				headers : {
            				    "X-My-Header" : "My Custom header"
            				}
                          },function(err,mail){
                         	    if (err){
                		        throw err;
                	        }
                	        else{
                	    	    let obj={
                	    		    message:'mail send to your valid email'
                	    	    }
                	    	    cb(null ,obj)
                	        }
                         })
                      }
                    })
         }
   }
   Account.remoteMethod(
		'registration',
		{
			description: 'Create an entity on podio system',
		    accepts: {arg: 'data',  type: 'object', required: true, http: {source: 'body'}},
		    http: {path: '/new/account/registration', verb: 'post'},
		    returns: {arg: 'registration_response', type: 'Array'}
   });
   //*****************************
   Account.loginMe = function(data, cb) {
      console.log('user ........'+JSON.stringify(data))
      Account.login(data, function(err, res) {
      	if (err){
          cb(null, err);
        }
        else {
          console.log('res.........'+JSON.stringify(res));
          Account.findById(res.userId, function (err, resp) {
            if (err) {
              throw err;
            }
            else {
            	resp.date_last_login=new Date();
            	resp.ip_last_login=data.ip_last_login;
            	resp.save(function(err,addressinfo){
            		if (err){
            			throw err;
            		}
            		cb(null, {
            		   userId:res.userId,
                       token: res.id
                    });
	    	    })
            }
          });
        }
      });
    };
    Account.remoteMethod('loginMe', {
      description: 'Logs user into the system',
      http: {path: '/login/new/api/', verb: 'post'},
      accepts: {arg: 'data',  type: 'object', required: true, http: {source: 'body'}},
      returns: {arg: 'result', type: 'Object'}
    });
    Account.getMe = function(id,cb) {
    	//let userId=params.id
      var ctx = LoopBackContext.getCurrentContext();
      var accessToken = ctx && ctx.get('accessToken');
      console.log('accessToken'+accessToken)
    	Account.findById(id,function(err, res){
        if (err){
          throw err;
        }
        else {
          cb(null, res);
        }
      });
    };
    Account.remoteMethod('getMe', {
      description: 'Get all info user',
      accepts: {arg: 'id', type: 'any', required: true},
      http: {path: '/me/:id', verb: 'get'},
      returns: {arg: 'result', type: 'Object'}
    });
    //****************************************************
    Account.updateAccount = function(id, data, cb) {
    	console.log('gfjhgkfghkl'+JSON.stringify(data))
        Account.update({id: id}, data, function(err, res) {
            if (err){
                throw err;
            }
            else {
                cb(null, res);
            }
        });
    };
    Account.remoteMethod('updateAccount', {
        description: 'update Account by userId',
        accepts: [
        {arg: 'id', type: 'any', required: true},
        {arg: 'data',  type: 'object', required: true, http: {source: 'body'}}
        ],
      http: {path: '/update/account/:id', verb: 'put'},
      returns: {arg: 'result', type: 'Object'}
    });
    
    Account.forgetPasswordEmail = function(data,cb) {
      let email = data.email;
      Account.findOne({where: { email: email } },function(err, res){
        if (err){
        	throw err;
        }
        if (!res){
        	let message="This email is not registered"
        	cb(message)
        }
        else {
        	var otp1= Math.floor( Math.random() * 900000) + 100000;
        	   var myMessage = {otp:otp1};
        	   var renderer = loopback.template(path.resolve(__dirname, '../../server/views/forget_password_template.ejs'));
               var html_body = renderer(myMessage);
               loopback.Email.send({
                    to: email,
					from: "clientes@woowbolivia.com",
					subject: "subject",
					text: "text message",
					html: html_body,
					//attachments : [path.resolve('../client/images/an-image.jpg')],
					var : {
					    myVar1 : 'a custom value'
					},
					headers : {
					    "X-My-Header" : "My Custom header"
					}
                },function(err,mail){
               	    if (err){
	    		        throw err;
	    	        }
	    	        else{
	    	        	res.forgetNumber=otp1
	    	        	res.save(function(err){
	    	        		if(err){
	    	        			throw err;
	    	        		}
	    	        		else{
	    	        			res.createAccessToken(5000, function(err, token) {
                                    console.log(token)
                                    let obj={
                                    	token:token.id,
                                    	userId:res.id,
                                	    message:'mail send to your valid email'
                                    }
                                    cb(null ,obj)
                                })
	    	        		}
	    	        	})
	    	        }
               })
	    	}
      })      
    }
    Account.remoteMethod('forgetPasswordEmail',{
      description: 'send forget password email',
      accepts: {arg: 'data',  type: 'object', required: true, http: {source: 'body'}},
      http: {path: '/forgetPassword', verb: 'post'},
      returns: {arg: 'result', type: 'Object'}
    });
    //*************************************
    Account.otpVerification = function(data,cb) {
    	//{where: { _id: email } }
      Account.findById(data.userId,function(err, res){
        if (err){
          throw err;
        }
        else {
        	if(res.forgetNumber==data.otp){
        		res.createAccessToken(5000, function(err, token) {
        			console.log(token)
        			let obj={
        				userId:res.id,
        				token:token.id,
        				message:"otp is verified"
        			}
        			cb(null ,obj)
        		})
        	}
        	else{
        		let message="You have entered wrong otp"
        		cb(null, message);
        	}

        }
      })      
    }
    Account.remoteMethod('otpVerification',{
      description: 'send forget password email',
      accepts: {arg: 'data',  type: 'object', required: true, http: {source: 'body'}},
      http: {path: '/otpVerification', verb: 'post'},
      returns: {arg: 'result', type: 'Object'}
    });
    //**************************************
    Account.resetPassword = function(data,cb) {
    	Account.findById(data.userId,function(err, res){
        if (err){
          throw err;
        }
        else {
        	res.password=data.newPass;
        	res.forgetNumber=Math.floor( Math.random() * 900000) + 100000;
        	res.save(function(err){
        		if (err){
        			throw err;
                }
                let message='your password has been changed sucessfully'
                cb(null,message)
        	})
        }
      })      
    }
    Account.remoteMethod('resetPassword',{
      description: 'send forget password email',
      accepts: {arg: 'data',  type: 'object', required: true, http: {source: 'body'}},
      http: {path: '/resetPassword', verb: 'post'},
      returns: {arg: 'result', type: 'Object'}
    });
};
